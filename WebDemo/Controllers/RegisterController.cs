﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebDemo.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string PostUsingParameter(string FirstName, string LastName)
        {
            return "from paramenter - " + FirstName + " " + LastName;
        }
    }
}